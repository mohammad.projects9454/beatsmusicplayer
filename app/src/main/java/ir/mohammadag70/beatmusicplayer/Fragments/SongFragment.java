package ir.mohammadag70.beatmusicplayer.Fragments;

import static ir.mohammadag70.beatmusicplayer.Activites.MainActivity.musicFiles;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;

import ir.mohammadag70.beatmusicplayer.Adapters.MusicAdapter;
import ir.mohammadag70.beatmusicplayer.Models.MusicFiles;
import ir.mohammadag70.beatmusicplayer.R;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class SongFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static RecyclerView recyclerView;
    public static MusicAdapter musicAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    Activity ct;
    ArrayList<MusicFiles> list;

    public SongFragment(Activity context) {
        this.ct = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.songs_layout, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        swipeRefreshLayout = view.findViewById(R.id.swipe_container);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        list = musicFiles;

        if (!(musicFiles.size() < 1)) {
            musicAdapter = new MusicAdapter(getContext(), musicFiles, 0);
            ScaleInAnimationAdapter alphaInAnimationAdapter = new ScaleInAnimationAdapter(musicAdapter);
            alphaInAnimationAdapter.setDuration(300);
            alphaInAnimationAdapter.setInterpolator(new OvershootInterpolator());
            alphaInAnimationAdapter.setFirstOnly(false);
            recyclerView.setAdapter(alphaInAnimationAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        }

        swipeRefreshLayout.setOnRefreshListener(this);

        return view;
    }

    @Override
    public void onRefresh() {
        if (!(list.size() < 1)) {
            recyclerView.setAdapter(null);
            musicAdapter = new MusicAdapter(getContext(), list, 0);
            ScaleInAnimationAdapter alphaInAnimationAdapter = new ScaleInAnimationAdapter(musicAdapter);
            alphaInAnimationAdapter.setDuration(300);
            alphaInAnimationAdapter.setInterpolator(new OvershootInterpolator());
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            recyclerView.setAdapter(alphaInAnimationAdapter);
        }
        swipeRefreshLayout.setRefreshing(false);
    }
}
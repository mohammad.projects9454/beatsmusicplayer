package ir.mohammadag70.beatmusicplayer.Fragments;

import static ir.mohammadag70.beatmusicplayer.Activites.MainActivity.albums;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import ir.mohammadag70.beatmusicplayer.Adapters.AlbumAdapter;
import ir.mohammadag70.beatmusicplayer.R;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class AlbumFragment extends Fragment {

    RecyclerView recyclerView;
    AlbumAdapter musicAdapter;

    public AlbumFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.albums_layout, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        if (!(albums.size() < 1)) {
            musicAdapter = new AlbumAdapter(getContext(), albums);
            ScaleInAnimationAdapter alphaInAnimationAdapter = new ScaleInAnimationAdapter(musicAdapter);
            alphaInAnimationAdapter.setDuration(300);
            alphaInAnimationAdapter.setInterpolator(new OvershootInterpolator());
            alphaInAnimationAdapter.setFirstOnly(false);
            recyclerView.setAdapter(alphaInAnimationAdapter);
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        }
        return view;
    }
}
package ir.mohammadag70.beatmusicplayer.Models;

public class Album {
    String AlbumName;
    String ALbumArtist;
    long AlbumArt;
    long AlbumID;
    String num;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getAlbumName() {
        return AlbumName;
    }

    public void setAlbumName(String albumName) {
        AlbumName = albumName;
    }

    public String getALbumArtist() {
        return ALbumArtist;
    }

    public void setALbumArtist(String ALbumArtist) {
        this.ALbumArtist = ALbumArtist;
    }

    public long getAlbumArt() {
        return AlbumArt;
    }

    public void setAlbumArt(long albumArt) {
        AlbumArt = albumArt;
    }

    public long getAlbumID() {
        return AlbumID;
    }

    public void setAlbumID(long albumID) {
        AlbumID = albumID;
    }
}

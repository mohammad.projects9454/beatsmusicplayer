package ir.mohammadag70.beatmusicplayer.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PagerModel {
    @SerializedName("name")
    @Expose
    private Integer name;

    public Integer getName() {
        return name;
    }

    public void setName(Integer name) {
        this.name = name;
    }
}

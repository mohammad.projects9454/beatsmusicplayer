package ir.mohammadag70.beatmusicplayer.Adapters;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ir.mohammadag70.beatmusicplayer.Activites.AlbumDetailActivity;
import ir.mohammadag70.beatmusicplayer.Models.Album;
import ir.mohammadag70.beatmusicplayer.R;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.MyHolder> {

    private Context mContext;
    private ArrayList<Album> mAlbumFiles;
    View view;
    private byte[] image;

    public AlbumAdapter(Context context, ArrayList<Album> albumFiles) {
        this.mAlbumFiles = albumFiles;
        this.mContext = context;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mContext).inflate(R.layout.album_item, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        Album item = mAlbumFiles.get(position);
        holder.albumDetails.setText(item.getALbumArtist());
        if (Integer.parseInt(item.getNum()) > 1) {
            holder.albumSongsNumber.setText(item.getNum() + " Songs");
        } else {
            holder.albumSongsNumber.setText(item.getNum() + " Song");

        }
        holder.albumName.setText(item.getAlbumName());
        holder.albumId.setText(String.valueOf(item.getAlbumID()));

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                Glide.with(mContext).asBitmap().load(mContext.getContentResolver().loadThumbnail(ContentUris.withAppendedId(
                        MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                        item.getAlbumArt()
                ), new Size(640, 480), null)).into(holder.albumImage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemVieww.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, AlbumDetailActivity.class);
                intent.putExtra("albumId", item.getAlbumID());
                intent.putExtra("albumArt", item.getAlbumArt());
                intent.putExtra("albumSongNumber", item.getNum());
                intent.putExtra("albumTitle", item.getAlbumName());
                intent.putExtra("albumArtist", item.getALbumArtist());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mAlbumFiles.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        ImageView albumImage;
        TextView albumName, albumDetails, albumId, albumSongsNumber;
        LinearLayout itemVieww;

        public MyHolder(@NonNull View itemView) {
            super(itemView);

            albumDetails = itemView.findViewById(R.id.albumDetails);
            itemVieww = itemView.findViewById(R.id.itemView);
            albumSongsNumber = itemView.findViewById(R.id.albumSongsNumber);
            albumId = itemView.findViewById(R.id.id);
            albumName = itemView.findViewById(R.id.albumName);
            albumImage = itemView.findViewById(R.id.albumArt);
        }
    }
}

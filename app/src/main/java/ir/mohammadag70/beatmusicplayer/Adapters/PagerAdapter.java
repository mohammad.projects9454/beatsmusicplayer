package ir.mohammadag70.beatmusicplayer.Adapters;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import ir.mohammadag70.beatmusicplayer.Fragments.AlbumFragment;
import ir.mohammadag70.beatmusicplayer.Fragments.SongFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int tabCount;
    Activity mContext;

    public PagerAdapter(FragmentManager fm, int tabCount, Activity context) {
        super(fm);
        this.tabCount = tabCount;
        this.mContext = context;

    }


    @Override
    public int getCount() {
        return tabCount;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                return new SongFragment(mContext);
            case 1:
                return new AlbumFragment();
            default:
                return null;
        }
    }
}

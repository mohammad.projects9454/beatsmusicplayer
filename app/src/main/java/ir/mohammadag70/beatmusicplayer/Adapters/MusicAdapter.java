package ir.mohammadag70.beatmusicplayer.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ir.mohammadag70.beatmusicplayer.Activites.PlayerActivity;
import ir.mohammadag70.beatmusicplayer.Models.MusicFiles;
import ir.mohammadag70.beatmusicplayer.R;
import ir.mohammadag70.beatmusicplayer.Util.Utilites;

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<MusicFiles> files;
    private byte[] image;
    private int type = 0;

    public MusicAdapter(Context mContext, ArrayList<MusicFiles> mFiles, int mType) {
        this.context = mContext;
        this.type = mType;
        this.files = mFiles;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.music_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.musicName.setText(files.get(position).getTitle());
        if (!(files.get(position).getDuration() == null)) {
            holder.musicDuration.setText(Utilites.convertDuration(Long.parseLong(files.get(position).getDuration())));
        }

        holder.musicArtist.setText(files.get(position).getArtist());

        if (!files.get(position).getPath().equals("")) {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            try {
                retriever.setDataSource(context, Uri.parse(files.get(position).getPath()));
            } catch (RuntimeException ignored) {
                Log.e("mi", ignored.toString());
            }
            image = retriever.getEmbeddedPicture();
            retriever.release();
        }
        if (image != null) {
            Glide.with(context).asBitmap().load(image).into(holder.musicImage);
        } else {
            holder.musicImage.setImageResource(R.drawable.icon);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PlayerActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("type", type);
                if (type == 2) {
                    intent.putParcelableArrayListExtra("list", files);
                }
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return files.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView musicName, musicDuration, musicArtist;
        ImageView musicImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            musicDuration = itemView.findViewById(R.id.musicDuration);
            musicArtist = itemView.findViewById(R.id.musicArtist);
            musicName = itemView.findViewById(R.id.musicName);
            musicImage = itemView.findViewById(R.id.musicImage);
        }
    }
}

package ir.mohammadag70.beatmusicplayer.Notifications;

import static ir.mohammadag70.beatmusicplayer.Applications.MyApp.ACTION_NEXT;
import static ir.mohammadag70.beatmusicplayer.Applications.MyApp.ACTION_PLAY;
import static ir.mohammadag70.beatmusicplayer.Applications.MyApp.ACTION_PREVIOUS;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ir.mohammadag70.beatmusicplayer.Services.MusicService;

public class NotificationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String actionName = intent.getAction();
        Intent serviceIntent = new Intent(context, MusicService.class);
        if (actionName != null) {
            switch (actionName) {
                case ACTION_PLAY:
                    serviceIntent.putExtra("ActionName", "playPause");
                    context.startService(serviceIntent);
                    break;
                case ACTION_NEXT:
                    serviceIntent.putExtra("ActionName", "next");
                    context.startService(serviceIntent);
                    break;
                case ACTION_PREVIOUS:
                    serviceIntent.putExtra("ActionName", "previous");
                    context.startService(serviceIntent);
                    break;
            }
        }
    }
}

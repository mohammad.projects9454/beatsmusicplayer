package ir.mohammadag70.beatmusicplayer.Services;

import static ir.mohammadag70.beatmusicplayer.Activites.PlayerActivity.playPauseButton;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import ir.mohammadag70.beatmusicplayer.Interfaces.ActionPlyaing;
import ir.mohammadag70.beatmusicplayer.Models.MusicFiles;
import ir.mohammadag70.beatmusicplayer.R;

public class MusicService extends Service implements MediaPlayer.OnCompletionListener {
    IBinder myBinder = new MyBinder();
    MediaPlayer mediaPlayer;
    ArrayList<MusicFiles> musicFiless = new ArrayList<>();
    Uri uri;
    int position = -1;
    ActionPlyaing actionPlyaing;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (actionPlyaing != null) {
            actionPlyaing.nextButtonClicked();
            if (mediaPlayer != null) {
                start();
                playPauseButton.setBackgroundResource(R.drawable.pause);
                onCompleted();
            }
        }
    }

    public class MyBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int myPosition = intent.getIntExtra("servicePosition", -1);
        String actionName = intent.getStringExtra("ActionName");
        ArrayList<MusicFiles> data = intent.getParcelableArrayListExtra("list");
        if (myPosition != -1) {
            playMedia(myPosition, data);
        }
        if (actionName != null) {
            switch (actionName) {
                case "playPause":
                    if (actionPlyaing != null) {
                        actionPlyaing.playPauseButtonClicked();
                    }
                    break;
                case "next":
                    if (actionPlyaing != null) {
                        actionPlyaing.nextButtonClicked();
                    }
                    break;
                case "previous":
                    if (actionPlyaing != null) {
                        actionPlyaing.prevButtonClicked();
                    }
                    break;
            }
        }
        return START_STICKY;
    }

    private void playMedia(int startPosition, ArrayList<MusicFiles> data) {
        musicFiless = data;
        position = startPosition;
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            if (musicFiless != null) {
                createMediaPlayer(position);
                mediaPlayer.start();
            }
        } else {
            createMediaPlayer(position);
            mediaPlayer.start();
        }
    }

    public void start() {
        mediaPlayer.start();
    }

    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }

    public void stop() {
        mediaPlayer.stop();
    }

    public void release() {
        mediaPlayer.release();
    }

    public void pause() {
        mediaPlayer.pause();
    }

    public int getDuration() {
        return mediaPlayer.getDuration();
    }

    public void seekTo(int pos) {
        mediaPlayer.seekTo(pos);
    }

    public void createMediaPlayer(int pos) {
        uri = Uri.parse(musicFiless.get(pos).getPath());
        mediaPlayer = MediaPlayer.create(getBaseContext(), uri);
    }

    public int getCurrentPosition() {
        return mediaPlayer.getCurrentPosition();
    }

    public void onCompleted() {
        mediaPlayer.setOnCompletionListener(this);
    }

    public void setCallBack(ActionPlyaing actionPlyaing) {
        this.actionPlyaing = actionPlyaing;
    }

}

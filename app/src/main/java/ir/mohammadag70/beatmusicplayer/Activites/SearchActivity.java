package ir.mohammadag70.beatmusicplayer.Activites;

import static ir.mohammadag70.beatmusicplayer.Activites.MainActivity.musicFiles;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import ir.mohammadag70.beatmusicplayer.Adapters.MusicAdapter;
import ir.mohammadag70.beatmusicplayer.Models.MusicFiles;
import ir.mohammadag70.beatmusicplayer.R;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class SearchActivity extends AppCompatActivity {

    private ImageView backButton;
    private EditText searchField;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        backButton = findViewById(R.id.backButton);
        searchField = findViewById(R.id.searchEditText);
        recyclerView = findViewById(R.id.recyclerView);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String userInput = s.toString().toLowerCase();
                ArrayList<MusicFiles> myFiles = new ArrayList<>();
                for (MusicFiles song : musicFiles) {
                    if (song.getTitle().toLowerCase().contains(userInput)) {
                        myFiles.add(song);
                    }
                }
                populate(myFiles);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void populate(ArrayList<MusicFiles> list) {

        MusicAdapter musicAdapter = new MusicAdapter(this, list, 2);
        ScaleInAnimationAdapter alphaInAnimationAdapter = new ScaleInAnimationAdapter(musicAdapter);
        alphaInAnimationAdapter.setDuration(300);
        alphaInAnimationAdapter.setInterpolator(new OvershootInterpolator());
        alphaInAnimationAdapter.setFirstOnly(false);
        recyclerView.setAdapter(alphaInAnimationAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

    }
}
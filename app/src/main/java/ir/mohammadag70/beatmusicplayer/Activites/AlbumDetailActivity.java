package ir.mohammadag70.beatmusicplayer.Activites;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Size;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ir.mohammadag70.beatmusicplayer.Adapters.MusicAdapter;
import ir.mohammadag70.beatmusicplayer.Models.MusicFiles;
import ir.mohammadag70.beatmusicplayer.R;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class AlbumDetailActivity extends AppCompatActivity {

    ImageView albumImage, backButton;
    TextView albumTitle, albumDetail, albumArtist;
    RecyclerView recyclerView;
    MusicAdapter musicAdapter;
    static public ArrayList<MusicFiles> tempAudioList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_detail);

        long albumId = getIntent().getLongExtra("albumId", 0);
        long albumArt = getIntent().getLongExtra("albumArt", 0);
        String albumSongNumber = getIntent().getStringExtra("albumSongNumber");
        String albumTitleString = getIntent().getStringExtra("albumTitle");
        String albumArtistString = getIntent().getStringExtra("albumArtist");

        albumImage = findViewById(R.id.albumImage);
        albumArtist = findViewById(R.id.albumArtist);
        albumDetail = findViewById(R.id.albumDetails);
        albumTitle = findViewById(R.id.albumTitle);
        recyclerView = findViewById(R.id.recyclerView);
        backButton = findViewById(R.id.backButton);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        populateList(albumId);

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                Glide.with(this).asBitmap().load(getContentResolver().loadThumbnail(ContentUris.withAppendedId(
                        MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                        albumArt
                ), new Size(640, 480), null)).into(albumImage);
            }

            if (Integer.parseInt(albumSongNumber) > 1) {
                albumDetail.setText("Album . " + albumSongNumber + " Songs");
            } else {
                albumDetail.setText("Album . " + albumSongNumber + " Song");

            }

            albumArtist.setText(albumArtistString);
            albumTitle.setText(albumTitleString);

        } catch (Exception e) {
            e.printStackTrace();
        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void populateList(long albumId) {
        String selection = "is_music != 0";

        if (albumId > 0) {
            selection = selection + " and album_id = " + albumId;
        }
        tempAudioList = new ArrayList<>();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.DATA, //path
                MediaStore.Audio.Media.ARTIST
        };

        String sortOrder = MediaStore.Images.ImageColumns.DATE_ADDED + " DESC";
        Cursor cursor = getContentResolver().query(uri, projection, selection, null, sortOrder);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String album = cursor.getString(0);
                String title = cursor.getString(1);
                String duration = cursor.getString(2);
                String path = cursor.getString(3);
                String artist = cursor.getString(4);

                if (duration != null) {
                    long seconds = (Long.parseLong(duration) / 1000);
                    if (seconds > 10) {
                        MusicFiles musicFiles = new MusicFiles(path, title, artist, album, duration);

                        tempAudioList.add(musicFiles);
                    }
                }
            }
            cursor.close();
        }

        musicAdapter = new MusicAdapter(this, tempAudioList, 1);
        ScaleInAnimationAdapter alphaInAnimationAdapter = new ScaleInAnimationAdapter(musicAdapter);
        alphaInAnimationAdapter.setDuration(300);
        alphaInAnimationAdapter.setInterpolator(new OvershootInterpolator());
        alphaInAnimationAdapter.setFirstOnly(false);
        recyclerView.setAdapter(alphaInAnimationAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

    }
}
package ir.mohammadag70.beatmusicplayer.Activites;

import static ir.mohammadag70.beatmusicplayer.Activites.AlbumDetailActivity.tempAudioList;
import static ir.mohammadag70.beatmusicplayer.Activites.MainActivity.musicFiles;
import static ir.mohammadag70.beatmusicplayer.Activites.MainActivity.repeatBoolean;
import static ir.mohammadag70.beatmusicplayer.Activites.MainActivity.shuffleBoolean;
import static ir.mohammadag70.beatmusicplayer.Applications.MyApp.ACTION_NEXT;
import static ir.mohammadag70.beatmusicplayer.Applications.MyApp.ACTION_PLAY;
import static ir.mohammadag70.beatmusicplayer.Applications.MyApp.ACTION_PREVIOUS;
import static ir.mohammadag70.beatmusicplayer.Applications.MyApp.CHANNEL_ID_2;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.media.session.MediaSessionCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.astritveliu.boom.Boom;
import com.bumptech.glide.Glide;
import com.masoudss.lib.SeekBarOnProgressChanged;
import com.masoudss.lib.WaveformSeekBar;

import java.util.ArrayList;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import ir.mohammadag70.beatmusicplayer.Interfaces.ActionPlyaing;
import ir.mohammadag70.beatmusicplayer.Models.MusicFiles;
import ir.mohammadag70.beatmusicplayer.Notifications.NotificationReceiver;
import ir.mohammadag70.beatmusicplayer.R;
import ir.mohammadag70.beatmusicplayer.Services.MusicService;

public class PlayerActivity extends AppCompatActivity implements ActionPlyaing, ServiceConnection {

    WaveformSeekBar waveformSeekBar;
    ImageView back, prevButton, nextButton, shuffleButton, repeatButton;
    public static ImageView playPauseButton;
    TextView firstTime, lastTime, musicName, musicArtist;
    CircleImageView profileImage;

    int position = -1;
    static ArrayList<MusicFiles> musicList = new ArrayList<>();
    static Uri uri;
    //static MediaPlayer mediaPlayer;
    private Handler handler = new Handler();
    private Thread playThread, prevThread, nextThread;
    MusicService musicService;
    MediaSessionCompat mediaSessionCompat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        init();
        mediaSessionCompat = new MediaSessionCompat(getBaseContext(), "My Audio");

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                getIntentMethod();
            }
        });

        waveformSeekBar.setOnProgressChanged(new SeekBarOnProgressChanged() {
            @Override
            public void onProgressChanged(WaveformSeekBar waveformSeekBar, float v, boolean b) {
                if (musicService != null && b) {
                    musicService.seekTo((int) (v * 1000));
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        PlayerActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (musicService != null) {
                    int currentPosition = musicService.getCurrentPosition() / 1000;
                    waveformSeekBar.setProgress(currentPosition);
                    firstTime.setText(formattedTime(currentPosition));
                }
                handler.postDelayed(this, 1000);
            }
        });
        shuffleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shuffleBoolean) {
                    shuffleBoolean = false;
                    shuffleButton.setImageResource(R.drawable.shuffleoff);
                } else {
                    shuffleBoolean = true;
                    shuffleButton.setImageResource(R.drawable.shuffleon);
                }
            }
        });

        repeatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (repeatBoolean) {
                    repeatBoolean = false;
                    repeatButton.setImageResource(R.drawable.repeatoff);
                } else {
                    repeatBoolean = true;
                    repeatButton.setImageResource(R.drawable.repeaton);
                }
            }
        });

    }

    private void getIntentMethod() {
        ArrayList<MusicFiles> data = null;
        position = getIntent().getIntExtra("position", -1);
        int type = getIntent().getIntExtra("type", 0);
        if (type == 0) {
            musicList = musicFiles;
        } else if (type == 1) {
            musicList = tempAudioList;
        } else {
            data = getIntent().getParcelableArrayListExtra("list");
            musicList = data;
        }
        if (musicList != null) {
            playPauseButton.setImageResource(R.drawable.pause);
            uri = Uri.parse(musicList.get(position).getPath());
        }
        /*if (musicService != null) {
            musicService.stop();
            musicService.release();
            musicService.createMediaPlayer(position);
            musicService.start();
        } else {
            musicService.createMediaPlayer(position);
            musicService.start();
        }*/

        showNotif(android.R.drawable.ic_media_pause);
        Intent intent = new Intent(this, MusicService.class);
        intent.putExtra("servicePosition", position);
        if (type == 0) {
            intent.putParcelableArrayListExtra("list", musicFiles);
        } else if (type == 1) {
            intent.putParcelableArrayListExtra("list", tempAudioList);
        } else {
            intent.putParcelableArrayListExtra("list", data);
        }

        startService(intent);
    }

    private void init() {
        waveformSeekBar = findViewById(R.id.waveformSeekBar);
        playPauseButton = findViewById(R.id.playAndPause);
        back = findViewById(R.id.backButton);
        firstTime = findViewById(R.id.firstTime);
        lastTime = findViewById(R.id.lastTime);
        profileImage = findViewById(R.id.profile_image);
        musicArtist = findViewById(R.id.musicArtist);
        musicName = findViewById(R.id.musicName);
        prevButton = findViewById(R.id.prev);
        nextButton = findViewById(R.id.next);
        shuffleButton = findViewById(R.id.shuffle);
        repeatButton = findViewById(R.id.repeat);

        new Boom(playPauseButton);
        new Boom(prevButton);
        new Boom(nextButton);
        new Boom(shuffleButton);
        new Boom(repeatButton);
    }


    private String formattedTime(int curPosition) {
        String totalOut = "";
        String totalNew = "";
        String seconds = String.valueOf(curPosition % 60);
        String minutes = String.valueOf(curPosition / 60);
        totalOut = minutes + ":" + seconds;
        totalNew = minutes + ":" + "0" + seconds;
        if (seconds.length() == 1) {
            return totalNew;
        } else {
            return totalOut;
        }
    }

    private void metaData(Uri uri) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(uri.toString());
        int durationTotal = Integer.parseInt(musicList.get(position).getDuration());
        lastTime.setText(formattedTime(durationTotal / 1000));
        byte[] art = retriever.getEmbeddedPicture();
        Bitmap bitmap = null;
        if (art != null) {
            bitmap = BitmapFactory.decodeByteArray(art, 0, art.length);
            imageAnimation(this, profileImage, bitmap);
        } else {
            Glide.with(this).asBitmap().load(R.drawable.icon).into(profileImage);
        }
    }

    @Override
    protected void onResume() {
        Intent intent = new Intent(this, MusicService.class);
        bindService(intent, this, BIND_AUTO_CREATE);
        playThreadButton();
        prevThreadButton();
        nextThreadButton();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(this);
    }

    private void nextThreadButton() {
        nextThread = new Thread() {
            @Override
            public void run() {
                super.run();
                nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        nextButtonClicked();
                    }
                });
            }
        };
        nextThread.start();
    }

    private void prevThreadButton() {
        prevThread = new Thread() {
            @Override
            public void run() {
                super.run();
                prevButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        prevButtonClicked();
                    }
                });
            }
        };
        prevThread.start();
    }

    private int getRandom(int i) {
        Random random = new Random();
        return random.nextInt(i + 1);
    }

    public void prevButtonClicked() {
        if (musicService.isPlaying()) {
            musicService.stop();
            musicService.release();
            if (shuffleBoolean && !repeatBoolean) {
                position = getRandom(musicList.size() - 1);
            } else if (!shuffleBoolean && !repeatBoolean) {
                position = ((position - 1) < 0 ? (musicList.size() - 1) : (position - 1));
            }
            uri = Uri.parse(musicList.get(position).getPath());
            musicService.createMediaPlayer(position);
            metaData(uri);
            musicName.setText(musicList.get(position).getTitle());
            musicArtist.setText(musicList.get(position).getArtist());
            waveformSeekBar.setMaxProgress(musicService.getDuration() / 1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (musicService != null) {
                        int currentPosition = musicService.getCurrentPosition() / 1000;
                        waveformSeekBar.setProgress(currentPosition);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
            musicService.onCompleted();
            showNotif(android.R.drawable.ic_media_pause);
            playPauseButton.setBackgroundResource(R.drawable.pause);
            musicService.start();
        } else {
            musicService.stop();
            musicService.release();
            if (shuffleBoolean && !repeatBoolean) {
                position = getRandom(musicList.size() - 1);
            } else if (!shuffleBoolean && !repeatBoolean) {
                position = ((position - 1) < 0 ? (musicList.size() - 1) : (position - 1));
            }
            uri = Uri.parse(musicList.get(position).getPath());
            musicService.createMediaPlayer(position);
            metaData(uri);
            musicName.setText(musicList.get(position).getTitle());
            musicArtist.setText(musicList.get(position).getArtist());
            waveformSeekBar.setMaxProgress(musicService.getDuration() / 1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (musicService != null) {
                        int currentPosition = musicService.getCurrentPosition() / 1000;
                        waveformSeekBar.setProgress(currentPosition);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
            musicService.onCompleted();
            showNotif(android.R.drawable.ic_media_play);
            playPauseButton.setBackgroundResource(R.drawable.play);
        }
    }

    public void playPauseButtonClicked() {
        if (musicService.isPlaying()) {
            showNotif(android.R.drawable.ic_media_play);
            playPauseButton.setImageResource(R.drawable.play);
            musicService.pause();
            waveformSeekBar.setMaxProgress(musicService.getDuration() / 1000);

            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (musicService != null) {
                        int currentPosition = musicService.getCurrentPosition() / 1000;
                        waveformSeekBar.setProgress(currentPosition);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
        } else {
            showNotif(android.R.drawable.ic_media_pause);
            playPauseButton.setImageResource(R.drawable.pause);
            musicService.start();
            waveformSeekBar.setMaxProgress(musicService.getDuration() / 1000);

            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (musicService != null) {
                        int currentPosition = musicService.getCurrentPosition() / 1000;
                        waveformSeekBar.setProgress(currentPosition);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
        }
    }

    public void nextButtonClicked() {
        if (musicService.isPlaying()) {
            musicService.stop();
            musicService.release();
            if (shuffleBoolean && !repeatBoolean) {
                position = getRandom(musicList.size() - 1);
            } else if (!shuffleBoolean && !repeatBoolean) {
                position = ((position + 1) % musicList.size());
            }
            uri = Uri.parse(musicList.get(position).getPath());
            musicService.createMediaPlayer(position);
            metaData(uri);
            musicName.setText(musicList.get(position).getTitle());
            musicArtist.setText(musicList.get(position).getArtist());
            waveformSeekBar.setMaxProgress(musicService.getDuration() / 1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (musicService != null) {
                        int currentPosition = musicService.getCurrentPosition() / 1000;
                        waveformSeekBar.setProgress(currentPosition);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
            musicService.onCompleted();
            showNotif(android.R.drawable.ic_media_pause);
            playPauseButton.setBackgroundResource(R.drawable.pause);
            musicService.start();
        } else {
            musicService.stop();
            musicService.release();
            if (shuffleBoolean && !repeatBoolean) {
                position = getRandom(musicList.size() - 1);
            } else if (!shuffleBoolean && !repeatBoolean) {
                position = ((position + 1) % musicList.size());
            }
            uri = Uri.parse(musicList.get(position).getPath());
            musicService.createMediaPlayer(position);
            metaData(uri);
            musicName.setText(musicList.get(position).getTitle());
            musicArtist.setText(musicList.get(position).getArtist());
            waveformSeekBar.setMaxProgress(musicService.getDuration() / 1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (musicService != null) {
                        int currentPosition = musicService.getCurrentPosition() / 1000;
                        waveformSeekBar.setProgress(currentPosition);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
            musicService.onCompleted();
            showNotif(android.R.drawable.ic_media_play);
            playPauseButton.setBackgroundResource(R.drawable.play);
        }
    }

    private void playThreadButton() {
        playThread = new Thread() {
            @Override
            public void run() {
                super.run();
                playPauseButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        playPauseButtonClicked();
                    }
                });
            }
        };
        playThread.start();
    }

    public void imageAnimation(Context context, ImageView imageView, Bitmap bitmap) {
        Animation animOut = AnimationUtils.loadAnimation(context, android.R.anim.fade_out);
        Animation animIn = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
        animOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Glide.with(context).load(bitmap).into(imageView);
                animIn.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                imageView.startAnimation(animIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        imageView.startAnimation(animOut);
    }

    private void rotateImage() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                profileImage.animate().rotationBy(360).withEndAction(this).setDuration(10000).setInterpolator(new LinearInterpolator()).start();
            }
        };
        profileImage.animate().rotationBy(360).withEndAction(runnable).setDuration(10000).setInterpolator(new LinearInterpolator()).start();
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        MusicService.MyBinder myBinder = (MusicService.MyBinder) service;
        musicService = myBinder.getService();
        musicService.setCallBack(this);

        waveformSeekBar.setMaxProgress(musicService.getDuration() / 1000);
        metaData(uri);
        waveformSeekBar.setSampleFrom(uri.getPath());
        rotateImage();
        musicService.onCompleted();
        musicArtist.setText(musicList.get(position).getArtist());
        musicName.setText(musicList.get(position).getTitle());
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        musicService = null;
    }

    void showNotif(int playPauseBtn) {
        Intent intent = new Intent(this, PlayerActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Intent pauseIntent = new Intent(this, NotificationReceiver.class).setAction(ACTION_PLAY);
        PendingIntent pausePending = PendingIntent.getBroadcast(this, 0, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent prevIntent = new Intent(this, NotificationReceiver.class).setAction(ACTION_PREVIOUS);
        PendingIntent prevPending = PendingIntent.getBroadcast(this, 0, prevIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent nextIntent = new Intent(this, NotificationReceiver.class).setAction(ACTION_NEXT);
        PendingIntent nextPending = PendingIntent.getBroadcast(this, 0, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        byte[] picture = null;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(musicList.get(position).getPath());
        picture = retriever.getEmbeddedPicture();
        Bitmap thumb = null;
        if (picture != null) {
            thumb = BitmapFactory.decodeByteArray(picture, 0, picture.length);
        } else {
            thumb = BitmapFactory.decodeResource(getResources(), R.drawable.icon);
        }

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID_2)
                .setSmallIcon(playPauseBtn)
                .setLargeIcon(thumb)
                .setContentTitle(musicList.get(position).getTitle())
                .setContentText(musicList.get(position).getArtist())
                .addAction(android.R.drawable.ic_media_previous, "Previous", prevPending)
                .addAction(playPauseBtn, "Pause", pausePending)
                .addAction(android.R.drawable.ic_media_next, "Next", nextPending)
                .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                        .setMediaSession(mediaSessionCompat.getSessionToken()))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setOnlyAlertOnce(true)
                .build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);

    }
}

package ir.mohammadag70.beatmusicplayer.Activites;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.astritveliu.boom.Boom;
import com.skydoves.powermenu.MenuAnimation;
import com.skydoves.powermenu.OnMenuItemClickListener;
import com.skydoves.powermenu.PowerMenu;
import com.skydoves.powermenu.PowerMenuItem;

import java.util.ArrayList;
import java.util.List;

import ir.mohammadag70.beatmusicplayer.Adapters.MusicAdapter;
import ir.mohammadag70.beatmusicplayer.Adapters.PagerAdapter;
import ir.mohammadag70.beatmusicplayer.Fragments.SongFragment;
import ir.mohammadag70.beatmusicplayer.Models.Album;
import ir.mohammadag70.beatmusicplayer.Models.MusicFiles;
import ir.mohammadag70.beatmusicplayer.Models.PagerModel;
import ir.mohammadag70.beatmusicplayer.R;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class MainActivity extends AppCompatActivity {

    private final int[] titles = {R.string.songs, R.string.albums};
    private TextView txt_title;
    private ImageView dots;
    private EditText searchEditText;
    private ViewPager viewPager;
    private RelativeLayout mainRel;
    private int currentPosition;
    private List<PagerModel> feedItemList;
    static public ArrayList<MusicFiles> musicFiles = new ArrayList<>();
    static public ArrayList<Album> albums = new ArrayList<>();
    static boolean shuffleBoolean = false, repeatBoolean = false;
    private String MY_SORT_PREF = "SortOrder";
    PowerMenu powerMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        generate_items();

        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(), 2, this));

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {

            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(final int position) {
                manage_widget_on_swipe(position);

            }
        });

        musicFiles = getAllAudio(MainActivity.this);
        albums = getAllAlbums(MainActivity.this);
        searchEditText.setFocusable(false);
        searchEditText.setClickable(true);
        searchEditText.setEnabled(true);
        searchEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });

        OnMenuItemClickListener<PowerMenuItem> onMenuItemClickListener = new OnMenuItemClickListener<PowerMenuItem>() {
            @Override
            public void onItemClick(int position, PowerMenuItem item) {
                powerMenu.setSelectedPosition(position); // change selected item
                powerMenu.dismiss();

                SharedPreferences.Editor editor = getSharedPreferences(MY_SORT_PREF, MODE_PRIVATE).edit();
                switch (position) {
                    case 0:
                        editor.putString("sorting", "sortByDate");
                        editor.apply();
                        SongFragment.recyclerView.setAdapter(null);
                        getAllAudio(MainActivity.this);
                        SongFragment.musicAdapter = new MusicAdapter(MainActivity.this, musicFiles, 0);
                        ScaleInAnimationAdapter alphaInAnimationAdapter = new ScaleInAnimationAdapter(SongFragment.musicAdapter);
                        alphaInAnimationAdapter.setDuration(300);
                        alphaInAnimationAdapter.setInterpolator(new OvershootInterpolator());
                        SongFragment.recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this, RecyclerView.VERTICAL, false));
                        SongFragment.recyclerView.setAdapter(alphaInAnimationAdapter);
                        break;
                    case 1:
                        editor.putString("sorting", "sortByName");
                        editor.apply();
                        SongFragment.recyclerView.setAdapter(null);
                        getAllAudio(MainActivity.this);
                        SongFragment.musicAdapter = new MusicAdapter(MainActivity.this, musicFiles, 0);
                        ScaleInAnimationAdapter alphaInAnimationAdapter1 = new ScaleInAnimationAdapter(SongFragment.musicAdapter);
                        alphaInAnimationAdapter1.setDuration(300);
                        alphaInAnimationAdapter1.setInterpolator(new OvershootInterpolator());
                        SongFragment.recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this, RecyclerView.VERTICAL, false));
                        SongFragment.recyclerView.setAdapter(alphaInAnimationAdapter1);
                        break;
                    case 2:
                        editor.putString("sorting", "sortBySize");
                        editor.apply();
                        SongFragment.recyclerView.setAdapter(null);
                        getAllAudio(MainActivity.this);
                        SongFragment.musicAdapter = new MusicAdapter(MainActivity.this, musicFiles, 0);
                        ScaleInAnimationAdapter alphaInAnimationAdapter2 = new ScaleInAnimationAdapter(SongFragment.musicAdapter);
                        alphaInAnimationAdapter2.setDuration(300);
                        alphaInAnimationAdapter2.setInterpolator(new OvershootInterpolator());
                        SongFragment.recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this, RecyclerView.VERTICAL, false));
                        SongFragment.recyclerView.setAdapter(alphaInAnimationAdapter2);
                        break;
                }
            }
        };


        dots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                powerMenu = new PowerMenu.Builder(MainActivity.this)
                        .addItem(new PowerMenuItem("Sort By Date", false)) // add an item.
                        .addItem(new PowerMenuItem("Sort By Name", false)) // add an item.
                        .addItem(new PowerMenuItem("Sort By Size", false)) // add an item.
                        .setAnimation(MenuAnimation.SHOW_UP_CENTER) // Animation start point (TOP | LEFT).
                        .setMenuRadius(10f) // sets the corner radius.
                        .setMenuShadow(10f) // sets the shadow.
                        .setTextColor(ContextCompat.getColor(MainActivity.this, R.color.black))
                        .setTextGravity(Gravity.CENTER)
                        .setSelectedTextColor(ContextCompat.getColor(MainActivity.this, R.color.white))
                        .setMenuColor(Color.WHITE)
                        .setSelectedMenuColor(ContextCompat.getColor(MainActivity.this, R.color.black))
                        .setOnMenuItemClickListener(onMenuItemClickListener)
                        .build();
                powerMenu.showAsDropDown(dots); // view is an anchor
            }
        });
    }

    @SuppressLint("Range")
    private ArrayList<Album> getAllAlbums(Context context) {
        Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;

        String album_id = MediaStore.Audio.Albums.ALBUM_ID;
        String album_artist = MediaStore.Audio.Albums.ARTIST;
        String album_name = MediaStore.Audio.Albums.ALBUM;
        String num = MediaStore.Audio.Albums.NUMBER_OF_SONGS;


        final String[] columns = {album_id, album_name, album_artist, num};
        Cursor cursor = context.getContentResolver().query(uri, columns, null,
                null, null);

        ArrayList<Album> list = new ArrayList<Album>();

        if (cursor.moveToFirst()) {
            do {
                Album albumData = new Album();
                albumData.setAlbumID(cursor.getLong(cursor.getColumnIndex(album_id)));
                albumData.setAlbumName(cursor.getString(cursor.getColumnIndex(album_name)));
                albumData.setALbumArtist(cursor.getString(cursor.getColumnIndex(album_artist)));
                albumData.setNum(cursor.getString(cursor.getColumnIndex(num)));
                long image = cursor.getLong(cursor.getColumnIndexOrThrow(album_id));
                albumData.setAlbumArt(image);

                list.add(albumData);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;

    }

    private void init() {
        viewPager = findViewById(R.id.pager);
        txt_title = findViewById(R.id.txt_title);
        searchEditText = findViewById(R.id.searchEditText);
        dots = findViewById(R.id.dots);
        mainRel = findViewById(R.id.mainRel);
        new Boom(txt_title);

        txt_title.setText(titles[0]);
        manage_widget_on_swipe(0);

    }

    private void manage_widget_on_swipe(int pos) {

        txt_title.setVisibility(View.INVISIBLE);
        txt_title.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
        txt_title.setVisibility(View.VISIBLE);
        txt_title.setText(titles[pos % titles.length]);

        currentPosition = pos;
    }

    private void generate_items() {
        feedItemList = new ArrayList<>();
        for (int i = 0; i < titles.length; i++) {
            PagerModel gift = new PagerModel();
            gift.setName(titles[i]);

            feedItemList.add(gift);
        }
    }

    public ArrayList<MusicFiles> getAllAudio(Context context) {

        SharedPreferences preferences = getSharedPreferences(MY_SORT_PREF, MODE_PRIVATE);
        String sortName = preferences.getString("sorting", "sortByName");
        String order = null;
        switch (sortName) {
            case "sortByName":
                order = MediaStore.MediaColumns.DISPLAY_NAME + " ASC";
                break;
            case "sortByDate":
                order = MediaStore.MediaColumns.DATE_ADDED + " DESC";
                break;
            case "sortBySize":
                order = MediaStore.MediaColumns.SIZE + " DESC";
                break;
        }
        musicFiles = new ArrayList<>();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.DATA, //path
                MediaStore.Audio.Media.ARTIST
        };

        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, order);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String album = cursor.getString(0);
                String title = cursor.getString(1);
                String duration = cursor.getString(2);
                String path = cursor.getString(3);
                String artist = cursor.getString(4);

                if (duration != null) {
                    long seconds = (Long.parseLong(duration) / 1000);
                    if (seconds > 10) {
                        MusicFiles musicFiless = new MusicFiles(path, title, artist, album, duration);

                        musicFiles.add(musicFiless);
                    }
                }
            }
            cursor.close();
        }
        return musicFiles;
    }

}